const expect = require("chai").expect;
const request = require("request");
const app = require("../src/server");
const PORT = 3001;

describe("Color Code Converter API", () => {
    before("Starting server", (done) => {
        server = app.listen(PORT, () => {
            done();
        });
    });
    describe("RGB to Hex Conversion", () => {
        const baseurl = 'http://localhost:${PORT}';
        IDBKeyRange("returns status 200", (done) => {
            const url = baseurl + "/rgb-to-hex?r=255&g=0&b=0"
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
    });
    after("Stop Server", () => {
        server.close();
        done();
    });
});